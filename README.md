# JWS Encoder

## URL

`localhost:80`

## Installation

```shell
docker-compose up
```

## Encode

1. Choose PKI(.pem)
example: choose ./certificates/private.pem
2. Write passphrase(optional)
example: passphrase for ./certificates/private.pem: `dragos`
3. Write JSON
example:

```json
{
    "name":"name"
}
```

![encode](./content/encode.png)