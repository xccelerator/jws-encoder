import { useState } from 'react'
import axios from 'axios';
import './App.css'

function App() {
  const [errorMessage, setErrorMessage] = useState('')
  const [encode, setEncode] = useState('')
  const [privateKeyFile, setPrivateKeyFile] = useState(null);
  const [jsonContent, setJsonContent] = useState('')
  const [passphrase, setPassphrase] = useState('')


  const clickSubmit = async () => {
    setEncode('')

    if(!jsonContent){
      setErrorMessage("Please provide JSON!")
      return
    }

    if(!privateKeyFile){
      setErrorMessage("Please provide private key!")
      return
    }
  
    const formData = new FormData();
    formData.append('privateKeyFile', privateKeyFile);
    formData.append('jsonInput', jsonContent);
    formData.append('passphrase', passphrase);

    await axios.post('http://localhost:5000/encode', formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
    }).then((response) => {
      if(response.data.signature){
        setErrorMessage('')
        setEncode(response.data.signature)
      }
      if(response.data.error){
        setEncode('')
        setErrorMessage(response.data.error)
      }
    })
  }

  const handleJsonChange = (e) => {
    setJsonContent(e.target.value)
  }

  const handleFileChange = (e) => {
    setPrivateKeyFile(e.target.files[0]);
  };

  const handlePassphraseChange = (e) => {
    setPassphrase(e.target.value);
  };

  return (
    <div className='codification-container'>
      <div className='codification-content-container'>
        <div className='project-name-container'>
            JWS Encoder
        </div>

        <div>
          <input 
            type="file" 
            accept=".pem" 
            onChange={handleFileChange} 
          />
        </div>

        <div>
          <label >
            Passphrase(optional):
            <input
              onChange={handlePassphraseChange}
            />
          </label>
        </div>

        <form className='json-text-area-container'>
          JSON:
          <textarea 
            className='json-text-area'
            placeholder='Input your JSON here...'
            onChange={handleJsonChange}
          />
        </form>

        { 
          errorMessage &&
          <div className='error-message-container'>
            {errorMessage}
          </div>
        }

        { 
          encode &&
          <div className='encode-message-container'>
            {encode}
          </div>
            
        }

        <button
          onClick={clickSubmit}
        >
          Convert
        </button>
      </div>
    </div>
  )
}

export default App
