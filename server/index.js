const express = require('express');
const multer = require('multer');
const jws = require('jws')
const cors = require('cors')


const app = express();
const port = 5000;

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

app.use(cors())
app.use(express.json());

app.post('/encode', upload.single('privateKeyFile'), async (req, res) => {
  try {
    const { jsonInput, passphrase } = req.body;
    const privateKey = req.file.buffer.toString();

    const secret = { key: privateKey, passphrase: passphrase };

    const signature = jws.sign({
        header: { alg: 'RS256' },
        payload: jsonInput,
        secret: secret
    })

    res.json({ signature });
  } catch (error) {
    console.error(error.message);
    res.json({ error: error.message });
  }
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
